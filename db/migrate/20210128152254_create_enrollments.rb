class CreateEnrollments < ActiveRecord::Migration[5.2]
  def change
    create_table :enrollments do |t|
      t.references :student, foreign_key: true
      t.references :classroom, foreign_key: true
      t.float :nota_um
      t.float :nota_dois

      t.timestamps
    end
  end
end
