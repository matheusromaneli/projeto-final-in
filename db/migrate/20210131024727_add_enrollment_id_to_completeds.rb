class AddEnrollmentIdToCompleteds < ActiveRecord::Migration[5.2]
  def change
    add_reference :completeds, :enrollment, foreign_key: true
  end
end
