class CreateAcademicperiods < ActiveRecord::Migration[5.2]
  def change
    create_table :academicperiods do |t|
      t.references :subjects, foreign_key: true
      t.integer :semester
      t.integer :year
      t.integer :status

      t.timestamps
    end
  end
end
