class AddAtributesToCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :name, :string
    add_column :courses, :knowledge_area, :string
    add_column :courses, :code, :string
    add_column :courses, :campus, :string
  end
end
