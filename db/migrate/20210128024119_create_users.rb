class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :nationality
      t.string :state
      t.string :rg
      t.string :cpf
      t.string :email
      t.string :password
      t.string :birthdate

      t.timestamps
    end
  end
end
