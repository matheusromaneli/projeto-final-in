class AddAtributesToDepartments < ActiveRecord::Migration[5.2]
  def change
    add_column :departments, :name, :string
    add_column :departments, :knowledge_area, :string
    add_column :departments, :code, :string
    add_column :departments, :campus, :string
  end
end
