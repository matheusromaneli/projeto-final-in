class CreatePrerequirements < ActiveRecord::Migration[5.2]
  def change
    create_table :prerequirements do |t|
      t.references :subject, foreign_key: true
      t.integer :requirement_id

      t.timestamps
    end
  end
end
