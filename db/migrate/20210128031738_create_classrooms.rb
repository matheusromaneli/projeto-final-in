class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.references :department, foreign_key: true
      t.references :teacher, foreign_key: true
      t.references :subject, foreign_key: true
      t.integer :vacancy
      t.string :name
      t.string :code
      t.string :room
      t.string :calendar

      t.timestamps
    end
  end
end
