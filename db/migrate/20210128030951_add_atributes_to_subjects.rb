class AddAtributesToSubjects < ActiveRecord::Migration[5.2]
  def change
    add_column :subjects, :name, :string
    add_column :subjects, :knowledge_area, :string
    add_column :subjects, :ch, :integer
  end
end
