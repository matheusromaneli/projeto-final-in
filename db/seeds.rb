# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name:"Romaneli",nationality:"Brasileiro",state:"RJ",rg:"12.123.123-1",cpf:"111.111.111-11",email:"diretor@id.uff.br",birthdate:"21-04-2020",password:"123456",role:4)
User.create(name:"Michel",nationality:"Brasileiro",state:"RJ",rg:"11.111.111-1",cpf:"222.222.222-22",email:"coordc@id.uff.br",birthdate:"21-04-2020",password:"123456",role:3)
User.create(name:"Matheus",nationality:"Brasileiro",state:"RJ",rg:"22.222.222-2",cpf:"333.333.333-33",email:"coordd@id.uff.br",birthdate:"21-04-2020",password:"123456",role:2)
Course.create(coordcourse_id:1,name:"Ciencia da Computaçao",knowledge_area:"Computaçao",code:"tcc01",campus:"Praia Vermelha")
User.create(name:"Mairon",nationality:"Brasileiro",state:"RJ",rg:"44.444.444-4",cpf:"555.555.555-55",email:"estudante@id.uff.br",birthdate:"21-04-2020",password:"123456",role:0)


Department.create(coorddepartment_id:1,name:"Instituto de Computaçao",knowledge_area:"Computaçao",code:"IC",campus:"Praia Vermelha")
User.create(name:"Amanda",nationality:"Brasileiro",state:"RJ",rg:"33.333.333-3",cpf:"444.444.444-44",email:"professor@id.uff.br",birthdate:"21-04-2020",password:"123456",role:1)
Subject.create(department_id:1,name:"Programaçao",knowledge_area:"Logica de programaçao",ch:60)
Classroom.create(department_id: 1,teacher_id: 1,subject_id: 1,vacancy: 40,name:"Turma",code:"F1",room:"302",calendar:"SegeQuar/14-16")
Academicperiod.create(semester:1,year:2021,status:0)
Enrollment.create(student_id:1,classroom_id:1,nota_um:9.0,nota_dois:9.0,status:0)
puts "critou"
Enrollment.create(student_id:1,classroom_id:1,nota_um:9.0,nota_dois:9.0,status:0)
puts "critou"
Enrollment.create(student_id:1,classroom_id:1,nota_um:9.0,nota_dois:9.0,status:0)
puts "critou"
Enrollment.create(student_id:1,classroom_id:1,nota_um:9.0,nota_dois:9.0,status:0)
Enrollment.create(student_id:1,classroom_id:1,nota_um:6.0,nota_dois:6.0,status:0)
Enrollment.create(student_id:1,classroom_id:1,nota_um:3.0,nota_dois:2.0,status:0)
