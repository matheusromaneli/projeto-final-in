# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_31_041646) do

  create_table "academicperiods", force: :cascade do |t|
    t.integer "subjects_id"
    t.integer "semester"
    t.integer "year"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subjects_id"], name: "index_academicperiods_on_subjects_id"
  end

  create_table "classrooms", force: :cascade do |t|
    t.integer "department_id"
    t.integer "teacher_id"
    t.integer "subject_id"
    t.integer "vacancy"
    t.string "name"
    t.string "code"
    t.string "room"
    t.string "calendar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_classrooms_on_department_id"
    t.index ["subject_id"], name: "index_classrooms_on_subject_id"
    t.index ["teacher_id"], name: "index_classrooms_on_teacher_id"
  end

  create_table "completeds", force: :cascade do |t|
    t.integer "student_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "enrollment_id"
    t.index ["enrollment_id"], name: "index_completeds_on_enrollment_id"
    t.index ["student_id"], name: "index_completeds_on_student_id"
    t.index ["subject_id"], name: "index_completeds_on_subject_id"
  end

  create_table "coordcourses", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_coordcourses_on_user_id"
  end

  create_table "coorddepartments", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_coorddepartments_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.integer "coordcourse_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "knowledge_area"
    t.string "code"
    t.string "campus"
    t.index ["coordcourse_id"], name: "index_courses_on_coordcourse_id"
  end

  create_table "departments", force: :cascade do |t|
    t.integer "coorddepartment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "knowledge_area"
    t.string "code"
    t.string "campus"
    t.index ["coorddepartment_id"], name: "index_departments_on_coorddepartment_id"
  end

  create_table "diretors", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_diretors_on_user_id"
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer "student_id"
    t.integer "classroom_id"
    t.float "nota_um"
    t.float "nota_dois"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.float "final_nota"
    t.index ["classroom_id"], name: "index_enrollments_on_classroom_id"
    t.index ["student_id"], name: "index_enrollments_on_student_id"
  end

  create_table "licenses", force: :cascade do |t|
    t.integer "teacher_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subject_id"], name: "index_licenses_on_subject_id"
    t.index ["teacher_id"], name: "index_licenses_on_teacher_id"
  end

  create_table "prerequirements", force: :cascade do |t|
    t.integer "subject_id"
    t.integer "requirement_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subject_id"], name: "index_prerequirements_on_subject_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "completed_id"
    t.index ["completed_id"], name: "index_students_on_completed_id"
    t.index ["course_id"], name: "index_students_on_course_id"
    t.index ["user_id"], name: "index_students_on_user_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.integer "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "knowledge_area"
    t.integer "ch"
    t.index ["department_id"], name: "index_subjects_on_department_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.integer "user_id"
    t.integer "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_teachers_on_department_id"
    t.index ["user_id"], name: "index_teachers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "nationality"
    t.string "state"
    t.string "rg"
    t.string "cpf"
    t.string "email"
    t.string "birthdate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.integer "role"
    t.string "street"
    t.string "number"
    t.string "neighborhood"
    t.string "complement"
    t.string "cep"
    t.string "telephone"
    t.string "cellphone"
  end

end
