require 'test_helper'

class AcademicperiodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academicperiod = academicperiods(:one)
  end

  test "should get index" do
    get academicperiods_url, as: :json
    assert_response :success
  end

  test "should create academicperiod" do
    assert_difference('Academicperiod.count') do
      post academicperiods_url, params: { academicperiod: { semester: @academicperiod.semester, status: @academicperiod.status, subjects_id: @academicperiod.subjects_id, year: @academicperiod.year } }, as: :json
    end

    assert_response 201
  end

  test "should show academicperiod" do
    get academicperiod_url(@academicperiod), as: :json
    assert_response :success
  end

  test "should update academicperiod" do
    patch academicperiod_url(@academicperiod), params: { academicperiod: { semester: @academicperiod.semester, status: @academicperiod.status, subjects_id: @academicperiod.subjects_id, year: @academicperiod.year } }, as: :json
    assert_response 200
  end

  test "should destroy academicperiod" do
    assert_difference('Academicperiod.count', -1) do
      delete academicperiod_url(@academicperiod), as: :json
    end

    assert_response 204
  end
end
