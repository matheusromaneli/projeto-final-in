require 'test_helper'

class DiretorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @diretor = diretors(:one)
  end

  test "should get index" do
    get diretors_url, as: :json
    assert_response :success
  end

  test "should create diretor" do
    assert_difference('Diretor.count') do
      post diretors_url, params: { diretor: { user_id: @diretor.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show diretor" do
    get diretor_url(@diretor), as: :json
    assert_response :success
  end

  test "should update diretor" do
    patch diretor_url(@diretor), params: { diretor: { user_id: @diretor.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy diretor" do
    assert_difference('Diretor.count', -1) do
      delete diretor_url(@diretor), as: :json
    end

    assert_response 204
  end
end
