require 'test_helper'

class CoorddepartmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coorddepartment = coorddepartments(:one)
  end

  test "should get index" do
    get coorddepartments_url, as: :json
    assert_response :success
  end

  test "should create coorddepartment" do
    assert_difference('Coorddepartment.count') do
      post coorddepartments_url, params: { coorddepartment: { department_id: @coorddepartment.department_id, user_id: @coorddepartment.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show coorddepartment" do
    get coorddepartment_url(@coorddepartment), as: :json
    assert_response :success
  end

  test "should update coorddepartment" do
    patch coorddepartment_url(@coorddepartment), params: { coorddepartment: { department_id: @coorddepartment.department_id, user_id: @coorddepartment.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy coorddepartment" do
    assert_difference('Coorddepartment.count', -1) do
      delete coorddepartment_url(@coorddepartment), as: :json
    end

    assert_response 204
  end
end
