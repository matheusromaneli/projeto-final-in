require 'test_helper'

class CoordcoursesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coordcourse = coordcourses(:one)
  end

  test "should get index" do
    get coordcourses_url, as: :json
    assert_response :success
  end

  test "should create coordcourse" do
    assert_difference('Coordcourse.count') do
      post coordcourses_url, params: { coordcourse: { course_id: @coordcourse.course_id, user_id: @coordcourse.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show coordcourse" do
    get coordcourse_url(@coordcourse), as: :json
    assert_response :success
  end

  test "should update coordcourse" do
    patch coordcourse_url(@coordcourse), params: { coordcourse: { course_id: @coordcourse.course_id, user_id: @coordcourse.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy coordcourse" do
    assert_difference('Coordcourse.count', -1) do
      delete coordcourse_url(@coordcourse), as: :json
    end

    assert_response 204
  end
end
