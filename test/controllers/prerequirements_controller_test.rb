require 'test_helper'

class PrerequirementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @prerequirement = prerequirements(:one)
  end

  test "should get index" do
    get prerequirements_url, as: :json
    assert_response :success
  end

  test "should create prerequirement" do
    assert_difference('Prerequirement.count') do
      post prerequirements_url, params: { prerequirement: { requirement_id: @prerequirement.requirement_id, subject_id: @prerequirement.subject_id } }, as: :json
    end

    assert_response 201
  end

  test "should show prerequirement" do
    get prerequirement_url(@prerequirement), as: :json
    assert_response :success
  end

  test "should update prerequirement" do
    patch prerequirement_url(@prerequirement), params: { prerequirement: { requirement_id: @prerequirement.requirement_id, subject_id: @prerequirement.subject_id } }, as: :json
    assert_response 200
  end

  test "should destroy prerequirement" do
    assert_difference('Prerequirement.count', -1) do
      delete prerequirement_url(@prerequirement), as: :json
    end

    assert_response 204
  end
end
