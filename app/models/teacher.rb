class Teacher < ApplicationRecord
  belongs_to :user
  belongs_to :department
  validates :user_id,uniqueness: true
end
