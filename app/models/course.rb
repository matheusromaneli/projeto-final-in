class Course < ApplicationRecord
  belongs_to :coordcourse
  has_many :students
  validates :name,:knowledge_area,:code,:campus,presence: true
end
