class Academicperiod < ApplicationRecord
  has_many :subjects
  enum status:{planning:0,registration:1,progress:2,concluded:3}
  validates :subjects_id, :semester, :year, :status,presence: true
end
