class Student < ApplicationRecord
  belongs_to :user
  belongs_to :course
  has_many :completeds
  validates :user_id,uniqueness: true
end
