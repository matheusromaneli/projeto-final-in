# frozen_string_literal: true
class Ability
  include CanCan::Ability
  def initialize(user)
    user ||= User.new

    if user.diretor?
      
      can :create, User, role:3
      can :create, User, role:2
      can :update, User, id: user.id
      can :manage, Course
      can :manage, Department
      can :manage, Coordcourse
      can :manage, Coorddepartment

    elsif user.coorddepartment?
      can :update, User, id: user.id
      can :read, :all
      can :update, Department
      can :create, Classroom
      can :manage, Subject
      can :create, Teacher
      can :create, Prerequirement

    elsif user.coordcourse?
      can :update, User, id: user.id
      can :manage, Course , id: Course.find(user.course_id)
      can :manage, Enrollment

    elsif user.teacher?

      can :read, Course
      can :update,Enrollment
      can :update, User, id:user.id
    elsif user.student?
      can :read, :all
      can :read, Course
      can :create, Enrollment
      can :update, Student, id: user.id
      can :update, User, role: 0

    else
      can :read,:all
    end
  end
  
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
end
