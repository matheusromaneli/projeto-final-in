class Enrollment < ApplicationRecord

  belongs_to :student
  belongs_to :classroom

  validate :able_to_do
  validate :full

  after_save :finish_entry
  enum status: {started: 0,reprovado: 1,aprovado:2}

  def finish_entry

    if self.nota_um != nil && self.nota_dois != nil && self.final_nota == nil
      self.final_nota = (nota_um + nota_dois)/2
      if final_nota>=6.0
        Completed.create(student_id: student_id,subject_id: Classroom.find(self.classroom_id).subject_id, enrollment_id: self.id)
        self.status=2
        self.save
      else
        self.status=1
        self.save
      end
    end
  end

  def able_to_do
    prequirement = Prerequirement.where(subject_id: Classroom.find(self.classroom_id).subject_id)
    for x in 0 .. prequirement.length - 1
      if Completed.find_by(student_id: student_id,subject_id: prequirement[x]['requirement_id']).nil?
        errors.add(:pre_requirement_not_met, "O estudante nao possui os pre-requisitos")
      end
    end
  end

  def full
    if(Classroom.find(classroom_id).vacancy < Enrollment.select{|e| e.classroom_id == classroom_id}.length)
      errors.add(:Classroom_already_full, "A turma já esta cheia, escolha outra")
    end
  end
end
