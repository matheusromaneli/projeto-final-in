class Classroom < ApplicationRecord
  belongs_to :department
  belongs_to :subject
  belongs_to :teacher
  has_many :enrollments
  validates :department_id, :teacher_id, :subject_id, :vacancy, :name, :code, :room, :calendar, presence: true

end
