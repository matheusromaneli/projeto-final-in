class Department < ApplicationRecord
  belongs_to :coorddepartment
  has_many :teachers
  has_many :subjects
  has_many :classrooms
end
