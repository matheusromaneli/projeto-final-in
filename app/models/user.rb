class User < ApplicationRecord
    has_secure_password
    
    validates :name, :nationality, :state, :birthdate, presence:true
    validates :rg,presence:true, uniqueness: true, format:{with: /\b[0-9]{2}+(.+[0-9]{3}){2}+-+[0-9]\z/, message: "O RG deve estar no formato 99.999.999-9"}
    validates :cpf, presence: true, uniqueness:true, format:{with: /\A([0-9]{3}+.){2}+[0-9]{3}+-+[0-9]{2}+\Z/ ,message:"Ex.: XXX.XXX.XXX-XX"}
    validates :email, presence: true, uniqueness:true,format:{with: /\b[A-Z0-9._%a-z\-]+@id.uff.br\Z/,message:"Ex.: example@id.uff.br"}
    enum role:{student:0,teacher:1,coordcourse:2,coorddepartment:3,diretor:4}
    
    after_create :set_role

    def set_role
        if self.student?
            Student.create(user_id: self.id,course_id: 1)#current_user.course_id)       
        elsif self.teacher?
            Teacher.create(user_id: self.id,department_id: 1)#current_user.department_id)
        elsif self.coordcourse?
            Coordcourse.create(user_id: self.id)
        elsif self.coorddepartment?
            Coorddepartment.create(user_id: self.id)
        elsif self.diretor?
            Diretor.create(user_id: self.id)
        end
    end
end
