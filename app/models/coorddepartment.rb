class Coorddepartment < ApplicationRecord
  belongs_to :user
  has_one :department
  validates :user_id,uniqueness: true
end
