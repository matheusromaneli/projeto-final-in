class License < ApplicationRecord
  belongs_to :teacher
  belongs_to :subject
  
  validates :teacher_id,:subject_id,presence: true
  validate :uniquelicense
  
  def uniquelicense
    if License.find_by(teacher_id: teacher_id,subject_id: subject_id).present?
      errors.add(:duplicate, "Licensa já existe")
    end
  end

end
