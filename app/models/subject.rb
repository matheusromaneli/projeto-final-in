class Subject < ApplicationRecord
  belongs_to :department
  validates :name, :knowledge_area, :ch,:department_id, presence: true
end
