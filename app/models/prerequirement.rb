class Prerequirement < ApplicationRecord
  belongs_to :subject

  validates :requirement_id ,presence: true
  
  validate :selfrequirement
  validate :duplicated
  validate :codependent
  validate :requirement_exists

  def selfrequirement
    if discipline_id == requirement_id
      errors.add(:same_discipline, "Uma matéria não pode ser pré-requisito de si mesma")
    end
  end

  def duplicated
    if PreRequirement.find_by(discipline_id: discipline_id,requirement_id: requirement_id)
      errors.add(:duplicate, "Pré-requisito já existe")
    end
  end

  def codependent
    if PreRequirement.find_by(discipline_id: requirement_id,requirement_id: discipline_id)
      errors.add(:pre_requirement_codependent, "Matérias não podem ser pré-requisitos entre si")
    end
  end
  
  def requirement_exists
    begin
      Discipline.find(requirement_id).present?
    rescue => exception
      errors.add(:pre_requirement_doesnt_exist, "Pré-requisito não existe")
    end
  end
end
