class PrerequirementsController < ApplicationController
  load_and_authorize_resource
  before_action :set_prerequirement, only: [:show, :update, :destroy]

  # GET /prerequirements
  def index
    @prerequirements = Prerequirement.all

    render json: @prerequirements
  end

  # GET /prerequirements/1
  def show
    render json: @prerequirement
  end

  # POST /prerequirements
  def create
    @prerequirement = Prerequirement.new(prerequirement_params)

    if @prerequirement.save
      render json: @prerequirement, status: :created, location: @prerequirement
    else
      render json: @prerequirement.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /prerequirements/1
  def update
    if @prerequirement.update(prerequirement_params)
      render json: @prerequirement
    else
      render json: @prerequirement.errors, status: :unprocessable_entity
    end
  end

  # DELETE /prerequirements/1
  def destroy
    @prerequirement.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prerequirement
      @prerequirement = Prerequirement.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def prerequirement_params
      params.require(:prerequirement).permit(:subject_id, :requirement_id)
    end
end
