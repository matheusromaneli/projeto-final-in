class AcademicperiodsController < ApplicationController
  load_and_authorize_resource
  before_action :set_academicperiod, only: [:show, :update, :destroy]

  # GET /academicperiods
  def index
    @academicperiods = Academicperiod.all

    render json: @academicperiods
  end

  # GET /academicperiods/1
  def show
    render json: @academicperiod
  end

  # POST /academicperiods
  def create
    @academicperiod = Academicperiod.new(academicperiod_params)

    if @academicperiod.save
      render json: @academicperiod, status: :created, location: @academicperiod
    else
      render json: @academicperiod.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /academicperiods/1
  def update
    if @academicperiod.update(academicperiod_params)
      render json: @academicperiod
    else
      render json: @academicperiod.errors, status: :unprocessable_entity
    end
  end

  # DELETE /academicperiods/1
  def destroy
    @academicperiod.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academicperiod
      @academicperiod = Academicperiod.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def academicperiod_params
      params.require(:academicperiod).permit(:subjects_id, :semester, :year, :status)
    end
end
