class AuthenticationController < ApplicationController

  def login
      user=User.find_by(cpf: params[:user][:cpf])
      user=user&.authenticate(params[:user][:password])
      if user
        token=JsonWebToken.encode([user_id: user.id])
        render json: {token: token,user: user}
      else
        render json: {message: "Usuário ou senha incorretos"}, status:401
      end
  end

  private

  def user_params
    params.require(:user).permit(:cpf, :password)
  end
  
end
