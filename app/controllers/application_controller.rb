class ApplicationController < ActionController::API

    rescue_from CanCan::AccessDenied do |exception|
        render json: {message:"Permissao Negada, voce nao tem acesso a esse conteúdo"}, status: 403
    end

    def current_user
        token = request.headers["Authorization"]
        token = token.split(" ").last if token.present?
        return nil unless token.present?
        decoded=JsonWebToken.decode(token)
        return nil unless decoded.present?
        puts decoded[0][0]["user_id"]
        User.find_by(id: decoded[0][0]["user_id"])
    end
    
end
