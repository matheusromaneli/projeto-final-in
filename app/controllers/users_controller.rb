class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    puts "entrou no create"
    @user = User.new(user_params)
    default_password(@user)
    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :nationality, :state, :rg, :cpf, :email, :password, :birthdate,:role,:street,:number,:neighborhood,:complement,:cep,:telephone,:cellphone)
    end
    
    def default_password(user)
      unless user.password.present?
          user.password="iduff000"
      end
    end
end
