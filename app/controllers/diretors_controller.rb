class DiretorsController < ApplicationController
  load_and_authorize_resource
  before_action :set_diretor, only: [:show, :update, :destroy]

  # GET /diretors
  def index
    @diretors = Diretor.all

    render json: @diretors
  end

  # GET /diretors/1
  def show
    render json: @diretor
  end

  # POST /diretors
  def create
    @diretor = Diretor.new(diretor_params)

    if @diretor.save
      render json: @diretor, status: :created, location: @diretor
    else
      render json: @diretor.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /diretors/1
  def update
    if @diretor.update(diretor_params)
      render json: @diretor
    else
      render json: @diretor.errors, status: :unprocessable_entity
    end
  end

  # DELETE /diretors/1
  def destroy
    @diretor.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diretor
      @diretor = Diretor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def diretor_params
      params.require(:diretor).permit(:user_id)
    end
end
