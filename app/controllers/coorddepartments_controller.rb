class CoorddepartmentsController < ApplicationController
  load_and_authorize_resource
  before_action :set_coorddepartment, only: [:show, :update, :destroy]

  # GET /coorddepartments
  def index
    @coorddepartments = Coorddepartment.all

    render json: @coorddepartments
  end

  # GET /coorddepartments/1
  def show
    render json: @coorddepartment
  end

  # POST /coorddepartments
  def create
    @coorddepartment = Coorddepartment.new(coorddepartment_params)

    if @coorddepartment.save
      render json: @coorddepartment, status: :created, location: @coorddepartment
    else
      render json: @coorddepartment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /coorddepartments/1
  def update
    if @coorddepartment.update(coorddepartment_params)
      render json: @coorddepartment
    else
      render json: @coorddepartment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /coorddepartments/1
  def destroy
    @coorddepartment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coorddepartment
      @coorddepartment = Coorddepartment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def coorddepartment_params
      params.require(:coorddepartment).permit(:user_id, :department_id)
    end
end
