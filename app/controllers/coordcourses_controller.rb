class CoordcoursesController < ApplicationController
  load_and_authorize_resource
  before_action :set_coordcourse, only: [:show, :update, :destroy]

  # GET /coordcourses
  def index
    @coordcourses = Coordcourse.all

    render json: @coordcourses
  end

  # GET /coordcourses/1
  def show
    render json: @coordcourse
  end

  # POST /coordcourses
  def create
    @coordcourse = Coordcourse.new(coordcourse_params)

    if @coordcourse.save
      render json: @coordcourse, status: :created, location: @coordcourse
    else
      render json: @coordcourse.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /coordcourses/1
  def update
    if @coordcourse.update(coordcourse_params)
      render json: @coordcourse
    else
      render json: @coordcourse.errors, status: :unprocessable_entity
    end
  end

  # DELETE /coordcourses/1
  def destroy
    @coordcourse.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coordcourse
      @coordcourse = Coordcourse.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def coordcourse_params
      params.require(:coordcourse).permit(:user_id, :course_id)
    end
end
