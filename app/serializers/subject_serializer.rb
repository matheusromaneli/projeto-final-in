class SubjectSerializer < ActiveModel::Serializer
  attributes :id,:name, :knowledge_area, :ch
  has_one :department
end
