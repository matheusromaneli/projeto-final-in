class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :knowledge_area,:code,:campus,:subscribers
  belongs_to :coordcourse
  def subscribers
    Student.select{|e| e.course_id == object.id}.length
  end

end
