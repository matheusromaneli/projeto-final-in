class AcademicperiodSerializer < ActiveModel::Serializer
  attributes :id, :period, :status
  has_many :subjects

  def period
    `#{self.year}+.+#{self.semester}`
  end
end
