class DepartmentSerializer < ActiveModel::Serializer
  attributes :id,:name, :knowledge_area, :code,:campus
  has_one :coorddepartment
  has_many :subjects
end
