class PrerequirementSerializer < ActiveModel::Serializer
  attributes :id, :requirement_id
  has_one :subject
end
