class TeacherSerializer < ActiveModel::Serializer
  
  attributes :name
  has_one :user
  has_one :department

  def name
    User.find(Teacher.find(object.id).user_id).name
  end
end
