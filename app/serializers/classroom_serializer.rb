class ClassroomSerializer < ActiveModel::Serializer
  attributes  :name,  :room,:code,:vacancy, :calendar
  has_one :department
  has_one :teacher
  has_one :subject

end
