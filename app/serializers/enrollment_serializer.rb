class EnrollmentSerializer < ActiveModel::Serializer
  attributes :id, :nota_um, :nota_dois,:final_nota,:status
  has_one :student
  has_one :classroom
end
