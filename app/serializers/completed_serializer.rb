class CompletedSerializer < ActiveModel::Serializer
  attributes :subname,:media,:status
  belongs_to :student

  def subname
    Subject.find(object.subject_id).name
  end

  def media
    Enrollment.find(object.enrollment_id).final_nota
  end

  def status
    Enrollment.find(object.enrollment_id).status
  end
end
