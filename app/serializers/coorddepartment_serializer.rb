class CoorddepartmentSerializer < ActiveModel::Serializer
  attributes :id
  has_one :user
  has_one :department
end
