class StudentSerializer < ActiveModel::Serializer
  attributes :name
  has_one :user
  has_many :completeds

  def name
    User.find(Student.find(object.id).user_id).name
  end

end
