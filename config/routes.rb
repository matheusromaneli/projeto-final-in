Rails.application.routes.draw do
  resources :academicperiods
  resources :completeds
  resources :prerequirements
  resources :enrollments
  resources :licenses
  post '/login',to: 'authentication#login'
  resources :classrooms
  resources :teachers
  resources :subjects
  resources :departments
  resources :students
  resources :courses
  resources :coorddepartments
  resources :coordcourses
  resources :diretors
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
